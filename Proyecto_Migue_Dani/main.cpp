
#include <windows.h>
#include <C:\GLUT\include\GL\glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <list>
#include <time.h>
#include <math.h>
#include "ImageLoader.h"
#define M_PI 3.14159265358979323846
using namespace std;

GLuint _pared; 


struct posEsfera{
int id;
double posx;
double posy;
double posz;
double tamanio;
double movx;
int direccionx;
double movy;
int direcciony;
double movz;
int direccionz;
};


GLuint loadTexture(Image* image) {
	GLuint idtextura;
	glGenTextures(1, &idtextura);
	glBindTexture(GL_TEXTURE_2D, idtextura);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return idtextura;
}


	void initRendering() {
		Image* lado1 = loadBMP("2.bmp");
		_pared = loadTexture(lado1);
		delete lado1;
		}

void cargarTextura(GLuint _textura) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textura);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void cuadro(){
	cargarTextura(_pared);
	    glBegin(GL_POLYGON);	
		  glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glEnd();
}


list <posEsfera> listaEsfereas;

list <double> burbujas;

GLfloat X = 0.0f;
GLfloat Y = 0.0f;
GLfloat Z = 0.0f;
double rotar = 0.0;
double rotary = 0.0;
float scale = 1.0f;

int pos[]={0,0,0,5,0,1,5,0,3,5,3,0,10,0,5,10,0,3,
           0,1,0,5,1,1,1,1,3,3,3,1,3,1,5,5,1,3,
           0,2,0,5,2,1,1,2,3,3,3,2,3,2,5,5,2,3,
           0,3,0,5,3,1,1,3,3,3,3,3,3,3,5,5,3,3,
           0,4,0,5,4,1,1,4,3,3,3,4,3,4,5,5,4,3,
           0,5,0,5,5,1,1,5,3,3,3,5,3,5,5,5,5,3};

void ArrowKey(int key, int x, int y) {
	//C�digo
	switch (key) {
	case GLUT_KEY_RIGHT:
		X += 0.05;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
		glRotatef(rotar,rotary, 0.1, 0);
		glMatrixMode(GL_MODELVIEW);
		break;

	case GLUT_KEY_LEFT:
			X -= 0.05;
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
			glRotatef(rotar,rotary, 0.1, 0);
			glMatrixMode(GL_MODELVIEW);
		break;
	case GLUT_KEY_UP:
		Z -= 0.05;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
		glRotatef(rotar,rotary, 0.1, 0);
		glMatrixMode(GL_MODELVIEW);
		break;
	case GLUT_KEY_DOWN:
			Z += 0.05;
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
			glRotatef(rotar,rotary, 0.1, 0);
			glMatrixMode(GL_MODELVIEW);
		break;
	}

	glutPostRedisplay();
}

void keyboard(unsigned char key,int x, int y){

        switch(key){
            case 'a':
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
			glRotatef(rotar, rotary, 0.1, 0);
            rotar-=0.5;

			glMatrixMode(GL_MODELVIEW);
            break;
            case 'd':
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
            glRotatef(rotar,rotary,0.1, 0);
            rotar+=0.5;

            glMatrixMode(GL_MODELVIEW);
            break;
            case 's':
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(85.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
            glRotatef(rotar,rotary,0.1, 0);
            glMatrixMode(GL_MODELVIEW);
            break;
            case 'w':
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(35.0,(GLfloat)1280/(GLfloat)720,2.0,128.0);
            glRotatef(rotar,rotary,0.1, 0);
            glMatrixMode(GL_MODELVIEW);
            break;
            case 'r':
            Y -= 0.02;
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
            glRotatef(rotar,rotary,0.1, 0);
            glMatrixMode(GL_MODELVIEW);
            break;
            case 'f':
            Y += 0.02;
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
            glRotatef(rotar,rotary,0.1, 0);
            glMatrixMode(GL_MODELVIEW);
            break;
            case'q':
            exit(0);
            break;
        }
}


void cargarPeces(int cantidad){

    int k=0;
    for(int i=0;i<cantidad;i++){
     struct posEsfera nuevo;
     nuevo.id=i;
     nuevo.posx=(pos[k]*0.20);
     nuevo.posy=(pos[k+1]*0.16);
     nuevo.posz=(pos[k+2]*0.16);
     nuevo.tamanio=0.1;
     nuevo.movx=0.00001;
     nuevo.direccionx=1;
     nuevo.movy=0.00001;
     nuevo.direcciony=1;
     nuevo.movz=0.00001;
     nuevo.direccionz=1;
     listaEsfereas.push_back(nuevo);
     k+=3;
     //cout<<"Creando esfera"<<endl;
    }
    srand(time(NULL));

    burbujas.push_back(1.820);
    burbujas.push_back(1.620);
    burbujas.push_back(1.420);
    burbujas.push_back(1.120);

    burbujas.push_back(0.95);
    burbujas.push_back(0.75);
    burbujas.push_back(0.55);
    burbujas.push_back(0.35);
    burbujas.push_back(0.15);


}

void dibujarPecera(){
//Dibujar Pecera**----------------------------------------------

    glBegin(GL_POLYGON);
    glColor3f(0.0,0.0,1.0);
    //Cara Abajo
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glEnd();
    //Cara izquierda
    glBegin(GL_POLYGON);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glEnd();
    //Cara derecha
    glBegin(GL_POLYGON);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glEnd();
    //Cara atras
    glBegin(GL_POLYGON);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glEnd();
    //Cara frente
    glBegin(GL_POLYGON);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glEnd();
    //Cara arriba
    glBegin(GL_POLYGON);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glEnd();
//Dibujar Base pecera**----------------------------------------------
//glPushMatrix();
glEnable(GL_DEPTH_TEST);
    glBegin(GL_POLYGON);
    glColor3f(1.0,1.0,0.0);
    //Cara Abajo
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glEnd();
    //Cara izquierda
    glBegin(GL_POLYGON);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glEnd();
    //Cara derecha
    glBegin(GL_POLYGON);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glEnd();
    //Cara atras
    glBegin(GL_POLYGON);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glEnd();
    //Cara frente
    glBegin(GL_POLYGON);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glEnd();
    //Cara arriba
    glBegin(GL_POLYGON);
    glColor3f(0.0,0.0,1.0);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glEnd();
    //glEnable(GL_AUTO_NORMAL);
    //glPopMatrix();
    glDisable(GL_DEPTH_TEST);

}

void dibujarMarcoPecera(){
    glColor3f(0.0,0.0,0.0);
    glBegin(GL_LINES);
    //Cara Abajo
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();
    //Cara izquierda
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();
    //Cara derecha
    glBegin(GL_LINES);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glEnd();
    //Cara atras
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();
    //Cara frente
    glBegin(GL_LINES);
    glVertex3f(3.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glEnd();
    //Cara arriba
    glBegin(GL_LINES);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 0.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(5.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 1.0f);
    glVertex3f(0.0f, 2.0f, 0.0f);
    glEnd();
//****************Mueble de la pecera*******
glEnable(GL_DEPTH_TEST);
    glBegin(GL_LINES);
    //Cara Abajo
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glEnd();
    //Cara izquierda
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();
    //Cara derecha
    glBegin(GL_LINES);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glEnd();
    //Cara atras
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(0.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, -2.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();
    //Cara frente
    glBegin(GL_LINES);
    glVertex3f(3.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(0.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, -2.0f, 1.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glEnd();
    //Cara arriba
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 0.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(5.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glEnd();
    glDisable(GL_DEPTH_TEST);
}

void dibujarBomba(){//***-----------------
GLUquadricObj *quadratic;
quadratic = gluNewQuadric();
glColor3f(0.0,0.0,0.0);
glPushMatrix();
glTranslatef(0.1,0.0,0.1);
glRotatef(-90, 1.0f, 0.0f, 0.0f);
gluCylinder(quadratic,0.1f,0.1f,1.0f,10,10);
glPopMatrix();

}

void dibujarPiramide(){
    //Abajo
    glPushMatrix();
    glTranslatef(2.270,0.070,0.4);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.270,0.070,0.55);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.270,0.070,0.7);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(2.420,0.070,0.4);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.420,0.070,0.55);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.420,0.070,0.7);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(2.570,0.070,0.4);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.570,0.070,0.55);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.570,0.070,0.7);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    //En medio
    glPushMatrix();
    glTranslatef(2.345,0.220,0.475);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.345,0.220,0.625);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    /*glPushMatrix();
    glTranslatef(1.345,0.220,0.7);
    glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();*/

    glPushMatrix();
    glTranslatef(2.495,0.220,0.475);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(2.495,0.220,0.625);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();
    /*glPushMatrix();
    glTranslatef(1.495,0.220,0.7);
    glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();*/
    //Arriba
    glPushMatrix();
    glTranslatef(2.42,0.350,0.55);
    //glColor3f(0.0,1.0,0.0);
    glutWireCube(0.15);
    glPopMatrix();

}
void resolverColisionPeces(){
    struct posEsfera aux,aux2;
    list <posEsfera> :: iterator it;
    list <posEsfera> :: iterator it2;
    double pitagoras=0;
    for(it = listaEsfereas.begin(); it != listaEsfereas.end(); ++it){
        aux = *it;
        for(it2 = listaEsfereas.begin(); it2 != listaEsfereas.end(); ++it2){
        aux2 = *it2;
                if(aux.id!=aux2.id){
                pitagoras=sqrt(pow((aux.posx-aux2.posx),2)+pow((aux.posy-aux2.posy),2)+pow((aux.posz-aux2.posz),2));
                    if(pitagoras<=0.3){
                        if(aux.direccionx!=aux2.direccionx){
                                (*it2).direccionx*=-1;
                                (*it).direccionx*=-1;
                        }else if(aux.direcciony!=aux2.direcciony){
                                (*it2).direcciony*=-1;
                                (*it).direcciony*=-1;
                        }else if(aux.direccionz!=aux2.direccionz){
                                (*it2).direccionz*=-1;
                                (*it).direccionz*=-1;
                        }
                    }
                }
        }
    }
}

void resolverColisionPecera(){
    struct posEsfera aux;
    list <posEsfera> :: iterator it;
    int update=rand()%(50000-1);
    for(it = listaEsfereas.begin(); it != listaEsfereas.end(); ++it){
        aux = *it;
        //cout<<(update<100)<<endl;
       if(aux.posx<0.1&&aux.posz<0.1){
         (*it).direccionz=1;
         (*it).direcciony=1;
         (*it).direcciony=1;
        }else if(update<2 && (*it).id%7==0){
            if(aux.posx>3.81){
                (*it).direccionx=-1;
            }else if(aux.posx<0.9){
                (*it).direccionx=1;
            }
            if(aux.posy>1.0){
                (*it).direcciony=-1;
            }else if(aux.posy<0.50){
                (*it).direcciony=1;
            }
            if(aux.posz>0.83){
                (*it).direccionz=-1;
            }else if(aux.posz<0.4){
                (*it).direccionz=1;
            }
        // resolverColisionPeces();
        }else{
          //  cout<<"No entro"<<endl;
        if(aux.posx>4.81){
            (*it).direccionx=-1;
        }else if(aux.posx<0.0){
            (*it).direccionx=1;
        }
        if(aux.posy>1.82){
            (*it).direcciony=-1;
        }else if(aux.posy<0.00){
            (*it).direcciony=1;
        }
        if(aux.posz>0.83){
            (*it).direccionz=-1;
        }else if(aux.posz<0.00){
            (*it).direccionz=1;
        }
        update=rand()%(1000-1);
        }



    }
}


void actualizarVelocidades(){
    struct posEsfera aux;
    list <posEsfera> :: iterator it;
    int update=rand()%(12-1);
    for(it = listaEsfereas.begin(); it != listaEsfereas.end(); ++it){
    if(update<6&&((*it).id%2)!=0){
    (*it).movx=(0.00001+(double)(rand()%(1001-1))/1000000)*(*it).direccionx;
    //cout<<"x:"<<(*it).movx<<endl;
    (*it).movy=(0.00001+(double)(rand()%(501-1))/1000000)*(*it).direcciony;
    //cout<<"y:"<<(*it).movy<<endl;
    (*it).movz=(0.00001+(double)(rand()%(701-1))/1000000)*(*it).direccionz;
    //cout<<"z:"<<(*it).movz<<endl;
    //cout<<"chale"<<endl;
    }else if(update>5&&update<<11&&((*it).id%2)==0){
     //cout<<"Entro"<<endl;
    (*it).movx=((double)(rand()%(2001-100))/1000000)*(*it).direccionx;
    //cout<<"x:"<<(*it).movx<<endl;
    (*it).movy=(0.00001+(double)(rand()%(1800-120))/10000000)*(*it).direcciony;
    //cout<<"y:"<<(*it).movy<<endl;
    (*it).movz=((double)(rand()%(2701-50))/10000000)*(*it).direccionz;
    }else if(update>10){
        (*it).movx=((double)(rand()%(2201-10))/10000)*(*it).direccionx;
    }
    update=rand()%(11-1);
    }
}

void dibujarPezEsfera(){
    struct posEsfera aux;
    list <posEsfera> :: iterator it;
    actualizarVelocidades();
    resolverColisionPeces();
    resolverColisionPecera();
    //resolverColisionPeces();
    for(it = listaEsfereas.begin(); it != listaEsfereas.end(); ++it){
		glPushMatrix();
		aux = *it;
		glTranslatef(0.080+aux.posx,0.080+aux.posy,0.080+aux.posz);
		glutWireSphere(0.040,10,10);
		(*it).posx+=aux.movx;
		(*it).posy+=aux.movy;
		(*it).posz+=aux.movz;
		glPopMatrix();
	}
    list <double> :: iterator it3;
    for(it3 = burbujas.begin(); it3 != burbujas.end(); ++it3){
        //cout<<"Entro"<<endl;
        //glPushMatrix();
        //cout<<(*it3)<<endl;
        if((*it3)>1.95){
            (*it3)=0.15;
        }

        if((*it3)>1.2){
            glPushMatrix();
                glTranslatef(0.080,0.080+(*it3),0.080);
                glScaled(1.60,1.60,1.60);
                glutWireSphere(0.040,10,10);
            glPopMatrix();
        }else if((*it3)>0.95){
            glPushMatrix();
                glTranslatef(0.080,0.080+(*it3),0.080);
                glScaled(1.30,1.30,1.30);
                glutWireSphere(0.040,10,10);
            glPopMatrix();
        }else{
            glPushMatrix();
                glTranslatef(0.080,0.080+(*it3),0.080);
                glutWireSphere(0.040,10,10);
            glPopMatrix();

        }

        (*it3)+=0.002;
        //glPopMatrix();
    }

        //glEnable(GL_DEPTH_TEST);
}

void reshape(int w, int h){//Se le pasa el ancho y el alto de la ventana, despues el rescalado
      glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    //(distancia h init, distancia v init,ancho,alto)
    glMatrixMode(GL_PROJECTION);//Especifica la matriz de tranformaciones sobre las que se realizan operaciones, para este caso cambioes en:
    glLoadIdentity();
    gluPerspective(60.0,(GLfloat)1280/(GLfloat)720,1.0,128.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //gluLookAt(4.0,2.0,5.0,3.0,0.0,0.0,0.0,1.0,0.0);
}

void display(void){//Funcion llamada para dibujar o redibujar la ventana cada cada vez que es necesario
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);//Se llama antes de pintar cualquier cosa y se le dice que buffer limpiara
    glClearColor(0.0,0.0,0.0,1.0);//Establece el color del fondo
    glLoadIdentity();
    gluLookAt(4.0+X,2.0-Y,5.0+Z,3.0+X,0.0-Y,0.0+Z,0.0,1.0,0.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
	glTranslatef(X,Y,Z);
	glPopMatrix();
    glColor3f(0.0,0.0,1.0);
	cuadro();
    dibujarPecera();
    dibujarMarcoPecera();
    dibujarBomba();
    dibujarPezEsfera();
    dibujarPiramide();

    glutSwapBuffers();//Intercambia el buffer posterior con el buffer anterior, por que trabaja con doble buffer
}


static void idle(void){
glutPostRedisplay();//Se le indica que de debe dibujar un figura, mas lo anterior quiere decir que cuando se a pulzado una tecla
}

int main(int argc, char** argv) {
	glutInit(&argc, argv); //Inicializamos la funciones de GLUT
	glutInitDisplayMode(GLUT_RGB|GLUT_DEPTH|GLUT_DOUBLE);//Se define el mode de como se va a dibujar la ventana
	glutInitWindowSize(1280,720);
	glutInitWindowPosition(20,20);
	cuadro();
    cargarPeces(30);
	glutCreateWindow("tecnunLogo");
	


	glutDisplayFunc(display);

    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);

    glutSpecialFunc(ArrowKey);
    glutMainLoop();// ejecuta ciclicamente nuestro proyecto
	return 0;
}
